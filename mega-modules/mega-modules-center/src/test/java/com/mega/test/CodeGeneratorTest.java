package com.mega.test;

import com.baomidou.mybatisplus.annotation.DbType;
import com.mega.common.generator.CodeGenerator;

public class CodeGeneratorTest {

    /**
     * 生成代码
     *
     * @param args
     */
    public static void main(String[] args) {
        CodeGenerator.builder()
                //表前缀
                .tablePrefix(new String[]{"sys_"})
                //表名
                .tableName(new String[]{"sys_dictionary","sys_dictionary_data","sys_i18n","sys_i18n_data"})
                //作者
                .author("Eric")
                //输出目录
                .outPutDir("D://home//tmp")
                //项目包路径
                .projectPackage("com.mega.center.system")
                //模块包路径
                .modulePackage("")
                //数据库配置
                .dbType(DbType.MYSQL)
                //数据库驱动
                .dbDriver("com.mysql.cj.jdbc.Driver")
                //数据库用户名
                .dbUsername("root")
                //数据库密码
                .dbPassword("123456")
                //数据库url
                .dbUrl("jdbc:mysql://localhost:3306/mega_platform?useUnicode=true&characterEncoding=utf8&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=GMT%2b8&createDatabaseIfNotExist=true")
                .build().execute();
    }
}