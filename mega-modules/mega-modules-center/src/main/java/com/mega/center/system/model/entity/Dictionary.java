package com.mega.center.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mega.common.core.web.model.entity.MegaBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典  实体
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_dictionary")
@ApiModel(value = "Dictionary对象", description = "字典 ")
public class Dictionary extends MegaBaseEntity<Dictionary> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "字典编码 英文标识")
    @TableField("DICT_CODE")
    private String dictCode;

    @ApiModelProperty(value = "字典名称 中文标识")
    @TableField("DICT_NAME")
    private String dictName;

    @ApiModelProperty(value = "状态")
    @TableField("DICT_STATUS")
    private String dictStatus;

    @ApiModelProperty(value = "字典类别 S：系统字典，U：用户字典")
    @TableField("DICT_CATEGORY")
    private String dictCategory;

    @ApiModelProperty(value = "父层级编码")
    @TableField("PARENT_DICT_CODE")
    private String parentDictCode;

    @ApiModelProperty(value = "显示顺序")
    @TableField("DICT_SORT")
    private Integer dictSort;

    @ApiModelProperty(value = "层级类型 M目录 D字典")
    @TableField("TYPE")
    private String type;

    @ApiModelProperty(value = "层级")
    @TableField("TREE_LEVEL")
    private Integer treeLevel;
}