package com.mega.center.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mega.center.system.model.entity.Menu;
import com.mega.center.system.model.vo.MenuTreeVO;
import com.mega.common.core.web.model.KeyValue;
import com.mega.common.core.web.model.TreeNodeSelect;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * 菜单表 Mapper接口
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * 批量插入
     *
     * @param menu 菜单表实体类
     * @return
     */
    Integer createBatch(List<Menu> menu);

    /**
     * 获取单个系统菜单
     *
     * @return
     */
    List<MenuTreeVO> listSystemAllMenus(String systemCode);

    /**
     * 获取单个系统菜单：选择组件
     *
     * @param systemCode
     * @return
     */
    List<TreeNodeSelect> listSystemAllMenusForSelect(String systemCode);

    /**
     * 获取用户菜单
     *
     * @param userName
     * @return
     */
    List<MenuTreeVO> listUserMenu(@Param("userName") String userName, @Param("userModuleCodes") Set<String> userModuleCodes);

    /**
     * 获取模块菜单
     *
     * @param userName
     * @return
     */
    List<MenuTreeVO> listMenuByModule(@Param("userModuleCodes") Set<String> userModuleCodes);

    /**
     * 获取用户菜单权限
     *
     * @param userName
     * @return
     */
    List<KeyValue> getUserMenuPermission(@Param("userName") String userName, @Param("userModuleCodes") Set<String> userModuleCodes);

}
