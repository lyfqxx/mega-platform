package com.mega.center.auth.enums;

/**
 * 登录日志枚举类
 */
public interface LoginLogEnums {

    enum LoginLogStatus {
        SUCCESS("0"),
        FAIL("1");

        String code;

        LoginLogStatus(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }
}
