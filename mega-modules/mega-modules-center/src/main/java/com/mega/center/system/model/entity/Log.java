package com.mega.center.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mega.common.core.web.model.entity.MegaBaseEntitySimple;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 操作日志 实体
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_log")
@ApiModel(value = "Log对象", description = "操作日志")
public class Log extends MegaBaseEntitySimple<Log> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "模块标题")
    @TableField("TITLE")
    private String title;

    @ApiModelProperty(value = "业务类型")
    @TableField("BUSINESS_TYPE")
    private String businessType;

    @ApiModelProperty(value = "方法名称")
    @TableField("METHOD")
    private String method;

    @ApiModelProperty(value = "请求方式")
    @TableField("REQUEST_METHOD")
    private String requestMethod;

    @ApiModelProperty(value = "操作类别（0其它 1后台用户 2手机端用户）")
    @TableField("OPERATOR_TYPE")
    private Integer operatorType;

    @ApiModelProperty(value = "操作人员")
    @TableField("OPER_NAME")
    private String operName;

    @ApiModelProperty(value = "部门名称")
    @TableField("DEPT_NAME")
    private String deptName;

    @ApiModelProperty(value = "请求URL")
    @TableField("OPER_URL")
    private String operUrl;

    @ApiModelProperty(value = "主机地址")
    @TableField("OPER_IP")
    private String operIp;

    @ApiModelProperty(value = "操作地点")
    @TableField("OPER_LOCATION")
    private String operLocation;

    @ApiModelProperty(value = "请求参数")
    @TableField("OPER_PARAM")
    private String operParam;

    @ApiModelProperty(value = "返回参数")
    @TableField("JSON_RESULT")
    private String jsonResult;

    @ApiModelProperty(value = "操作状态（0正常 1异常）")
    @TableField("STATUS")
    private Integer status;

    @ApiModelProperty(value = "错误消息")
    @TableField("ERROR_MSG")
    private String errorMsg;

    @ApiModelProperty(value = "操作时间")
    @TableField("OPER_TIME")
    private LocalDateTime operTime;

}