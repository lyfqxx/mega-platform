package com.mega.center.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mega.common.core.web.model.entity.MegaBaseEntityNone;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 角色菜单关联表 实体
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_role_menu")
@ApiModel(value = "RoleMenu对象", description = "角色菜单关联表")
public class RoleMenu extends MegaBaseEntityNone<RoleMenu> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "角色编码")
    @TableId("ROLE_CODE")
    private String roleCode;

    @ApiModelProperty(value = "菜单编码")
    @TableField("MENU_CODE")
    private String menuCode;
}