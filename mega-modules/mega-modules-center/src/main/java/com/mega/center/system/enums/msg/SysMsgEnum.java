package com.mega.center.system.enums.msg;

import com.mega.common.core.enums.BaseMsgEnum;


/**
 * 认证异常信息
 */
public enum SysMsgEnum implements BaseMsgEnum {

    //不允许操作超级管理员用户
    NO_RIGHT_OPER_ADMIN("NO_RIGHT_OPER_ADMIN"),
    //用户旧密码错误
    USER_OLDPWD_ERROR("USER_OLDPWD_ERROR"),
    //用户状态无效
    USER_STATUS_NOT_VALID("USER_STATUS_NOT_VALID"),
    //用户名已存在
    USER_NAME_REPEAT("USER_NAME_REPEAT"),
    //用户电话已存在
    USER_PHONE_REPEAT("USER_PHONE_REPEAT"),
    //用户邮箱已存在
    USER_EMAIL_REPEAT("USER_EMAIL_REPEAT"),


    //数据字典类型不能变更
    DICTIONARY_TYPE_CHANGE("DICTIONARY_TYPE_CHANGE"),
    //字典不允许有子节点
    DICTIONARY_HAS_CHILD("DICTIONARY_HAS_CHILD"),
    ;

    private String code;

    SysMsgEnum(String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return this.code;
    }
}
