package com.mega.center.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mega.center.system.model.dto.query.UserQueryDTO;
import com.mega.center.system.model.entity.User;
import com.mega.center.system.model.dto.ext.UserExt;
import com.mega.center.system.service.IUserService;
import com.mega.common.core.domain.R;
import com.mega.common.core.web.controller.MageBaseController;
import com.mega.common.log.annotation.SysLog;
import com.mega.common.log.enums.BusinessType;
import com.mega.common.security.utils.SecurityUtils;
import com.mega.system.api.vo.UserAuthInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 用户 前端控制器
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
@Slf4j
@RestController
@RequestMapping("/api/user")
@ApiModel(value = "用户", description = "用户")
@Api(tags = "用户")
public class UserController extends MageBaseController<User, IUserService> {

    /**
     * 分页查询用户
     *
     * @param param 查询参数
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "分页查询用户集合")
    public R<IPage<User>> listOfPage(UserQueryDTO param) {
        return R.ok(this.service.listOfPage(new Page<>(param.getPage(), param.getSize()), param));
    }

    /**
     * 新增用户
     *
     * @param user 用户实体类
     * @return R
     */
    @PostMapping
    @ApiOperation(value = "新增用户")
    @SysLog(title = "用户", businessType = BusinessType.INSERT)
    public R<Boolean> create(@RequestBody @Validated UserExt user) {
        service.create(user);
        return R.ok();
    }

    /**
     * 编辑用户
     *
     * @param user 用户实体类
     * @return R
     */
    @PutMapping
    @ApiOperation(value = "编辑用户")
    @SysLog(title = "用户", businessType = BusinessType.UPDATE)
    public R<Boolean> update(@RequestBody @Validated UserExt user) {
        service.update(user);
        return R.ok();
    }

    /**
     * 删除用户
     *
     * @param id 主键值
     * @return R
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除用户")
    @SysLog(title = "用户", businessType = BusinessType.DELETE)
    public R<Boolean> remove(@PathVariable Long id) {
        service.remove(id);
        return R.ok();
    }

    /**
     * 根据主键查找
     *
     * @param id 主键值
     * @return R
     */
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "根据主键查找用户")
    public R<User> getById(@PathVariable Integer id) {
        return R.ok(this.service.getById(id));
    }

    /**
     * 修改密码
     *
     * @param oldPwd
     * @param newPwd
     * @return
     */
    @PutMapping("/changePwd")
    @ApiOperation(value = "修改密码")
    @SysLog(title = "用户", businessType = BusinessType.UPDATE)
    public R<Boolean> changePwd(@RequestParam String oldPwd, @RequestParam String newPwd) {
        service.changePwd(SecurityUtils.getUserId(), oldPwd, newPwd);
        return R.ok();
    }

    /**
     * 重置密码
     *
     * @return
     */
    @PutMapping("/resetPwd")
    @ApiOperation(value = "重置密码")
    @SysLog(title = "用户", businessType = BusinessType.UPDATE)
    public R<Boolean> resetPwd() {
        service.resetPwd(SecurityUtils.getUserId());
        return R.ok();
    }

    /**
     * 修改用户状态
     *
     * @param status
     * @return
     */
    @PutMapping("/changeStatus/{status}")
    @ApiOperation(value = "修改用户状态")
    @SysLog(title = "用户", businessType = BusinessType.UPDATE)
    public R<Boolean> changeUserStatus(@PathVariable String status) {
        service.changeUserStatus(SecurityUtils.getUserId(), status);
        return R.ok();
    }

    /**
     * 获取用户及权限信息
     *
     * @return
     */
    @GetMapping("/info")
    @ApiOperation(value = "获取用户及权限信息")
    public R<UserAuthInfo> info() {
        return R.ok(service.getUserAuthInfoForLogin(SecurityUtils.getUsername()));
    }
}
