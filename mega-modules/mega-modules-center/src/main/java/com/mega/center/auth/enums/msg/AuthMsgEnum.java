package com.mega.center.auth.enums.msg;

import com.mega.common.core.enums.BaseMsgEnum;


/**
 * 认证异常信息
 */
public enum AuthMsgEnum implements BaseMsgEnum {

    //用户名/密码必填
    USER_PASSWORD_NOT_NULL("USER_PASSWORD_NOT_NULL"),
    //用户名长度必须在2到20个字符之间
    USER_USERNAME_LENGTH_NOT_VALID("USER_USERNAME_LENGTH_NOT_VALID"),
    //密码长度必须在5到20个字符之间
    USER_PASSWORD_LENGTH_NOT_VALID("USER_PASSWORD_LENGTH_NOT_VALID"),
    //用户不存在
    USER_NOT_EXISTS("USER_NOT_EXISTS"),
    //用户密码错误
    USER_PASSWORD_ERROR("USER_PASSWORD_ERROR"),
    //用户已封禁，请联系管理员
    USER_BLOCKED("USER_BLOCKED"),
    //登录成功
    USER_LOGIN_SUCCESS("USER_LOGIN_SUCCESS"),
    //退出成功
    USER_LOGOUT_SUCCESS("USER_LOGOUT_SUCCESS"),
    ;

    private String code;

    AuthMsgEnum(String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return this.code;
    }
}
