package com.mega.center.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mega.common.core.web.model.entity.MegaBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统模块  实体
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_module")
@ApiModel(value = "Module对象", description = "系统模块 ")
public class Module extends MegaBaseEntity<Module> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "模块编码")
    @TableField("MODULE_CODE")
    private String moduleCode;

    @ApiModelProperty(value = "模块名称")
    @TableField("MODULE_NAME")
    private String moduleName;

    @ApiModelProperty(value = "模块URL")
    @TableField("MODULE_URL")
    private String moduleUrl;

    @ApiModelProperty(value = "上级模块编码")
    @TableField("PARENT_MODULE_CODE")
    private String parentModuleCode;

    @ApiModelProperty(value = "上级模块编码集合")
    @TableField("PARENT_MODULE_CODES")
    private String parentModuleCodes;

    @ApiModelProperty(value = "树深度")
    @TableField("TREE_LEVEL")
    private Integer treeLevel;

    @ApiModelProperty(value = "排序")
    @TableField("TREE_SORT")
    private Integer treeSort;

    @ApiModelProperty(value = "状态：正常(NORMAL) 禁用(DISABLED)")
    @TableField("STATUS")
    private String status;

}