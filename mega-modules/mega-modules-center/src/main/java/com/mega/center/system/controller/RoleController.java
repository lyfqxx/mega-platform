package com.mega.center.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mega.center.system.model.dto.query.RoleQueryDTO;
import com.mega.center.system.model.entity.Role;
import com.mega.center.system.model.dto.ext.RoleExt;
import com.mega.center.system.service.IRoleService;
import com.mega.common.core.domain.R;
import com.mega.common.core.web.controller.MageBaseController;
import com.mega.common.log.annotation.SysLog;
import com.mega.common.log.enums.BusinessType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 角色表 前端控制器
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
@Slf4j
@RestController
@RequestMapping("/api/role")
@ApiModel(value = "角色表", description = "角色表")
@Api(tags = "角色表")
public class RoleController extends MageBaseController<Role, IRoleService> {

    /**
     * 分页查询角色表
     *
     * @param param 查询参数
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "分页查询角色表集合")
    public R<IPage<Role>> listOfPage(RoleQueryDTO param) {
        return R.ok(this.service.listOfPage(new Page<>(param.getPage(), param.getSize()), param));
    }

    /**
     * 新增角色表
     *
     * @param roleVo 角色表实体类
     * @return R
     */
    @PostMapping
    @ApiOperation(value = "新增角色表")
    @SysLog(title = "角色表", businessType = BusinessType.INSERT)
    public R<Boolean> create(@RequestBody @Validated RoleExt roleVo) {
        service.create(roleVo);
        return R.ok();
    }

    /**
     * 编辑角色表
     *
     * @param roleVo 角色表实体类
     * @return R
     */
    @PutMapping
    @ApiOperation(value = "编辑角色表")
    @SysLog(title = "角色表", businessType = BusinessType.UPDATE)
    public R<Boolean> update(@RequestBody @Validated RoleExt roleVo) {
        service.update(roleVo);
        return R.ok();
    }

    /**
     * 删除角色表
     *
     * @param id 主键值
     * @return R
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除角色表")
    @SysLog(title = "角色表", businessType = BusinessType.DELETE)
    public R<Boolean> remove(@PathVariable Integer id) {
        service.remove(id);
        return R.ok();
    }

    /**
     * 根据主键查找
     *
     * @param id 主键值
     * @return R
     */
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "根据主键查找角色表")
    public R<Role> getById(@PathVariable Integer id) {
        return R.ok(this.service.getById(id));
    }
}
