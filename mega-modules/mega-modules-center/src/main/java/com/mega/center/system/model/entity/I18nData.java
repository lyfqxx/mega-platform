package com.mega.center.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mega.common.core.web.model.entity.MegaBaseEntityNoDel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 国际化数据 实体
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_i18n_data")
@ApiModel(value = "I18nData对象", description = "国际化数据")
public class I18nData extends MegaBaseEntityNoDel<I18nData> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "编码")
    @TableField("I18N_CODE")
    private String i18nCode;

    @ApiModelProperty(value = "值")
    @TableField("DATA_VALUE")
    private String dataValue;

    @ApiModelProperty(value = "所属语言编码")
    @TableField("LANGUAGE_CODE")
    private String languageCode;

    @ApiModelProperty(value = "所属语言名称")
    @TableField("LANGUAGE_NAME")
    private String languageName;

}