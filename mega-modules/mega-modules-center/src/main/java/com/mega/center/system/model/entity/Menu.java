package com.mega.center.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mega.common.core.web.model.entity.MegaBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 菜单表 实体
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_menu")
@ApiModel(value = "Menu对象", description = "菜单表")
public class Menu extends MegaBaseEntity<Menu> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "菜单编码")
    @TableField("MENU_CODE")
    private String menuCode;

    @ApiModelProperty(value = "菜单名称")
    @TableField("MENU_NAME")
    private String menuName;

    @ApiModelProperty(value = "父菜单编码")
    @TableField("PARENT_MENU_CODE")
    private String parentMenuCode;

    @ApiModelProperty(value = "父菜单编码集")
    @TableField("PARENT_MENU_CODES")
    private String parentMenuCodes;

    @ApiModelProperty(value = "显示顺序")
    @TableField("MENU_SORT")
    private String menuSort;

    @ApiModelProperty(value = "路由地址")
    @TableField("MENU_PATH")
    private String menuPath;

    @ApiModelProperty(value = "组件")
    @TableField("COMPONENT")
    private String component;

    @ApiModelProperty(value = "是否外链")
    @TableField("IS_FRAME")
    private String isFrame;

    @ApiModelProperty(value = "菜单类型 M目录 C菜单 F按钮")
    @TableField("MENU_TYPE")
    private String menuType;

    @ApiModelProperty(value = "是否显示")
    @TableField("VISIBLE")
    private String visible;

    @ApiModelProperty(value = "权限标识")
    @TableField("PERMS")
    private String perms;

    @ApiModelProperty(value = "菜单图标")
    @TableField("ICON")
    private String icon;

    @ApiModelProperty(value = "所属系统编码")
    @TableField("SYSTEM_CODE")
    private String systemCode;

    @ApiModelProperty(value = "层级")
    @TableField("TREE_LEVEL")
    private Integer treeLevel;

}