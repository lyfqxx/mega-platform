package com.mega.center.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mega.common.core.web.model.entity.MegaBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色表 实体
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_role")
@ApiModel(value = "Role对象", description = "角色表")
public class Role extends MegaBaseEntity<Role> {

    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableField("ID")
    @TableId
    private Long id;

    @ApiModelProperty(value = "角色编码")
    @TableField("ROLE_CODE")
    private String roleCode;

    @ApiModelProperty(value = "角色名称")
    @TableField("ROLE_NAME")
    private String roleName;

    @ApiModelProperty(value = "角色分类")
    @TableField("ROLE_CATEGORY")
    private String roleCategory;

    @ApiModelProperty(value = "备注")
    @TableField("REMARK")
    private String remark;

}