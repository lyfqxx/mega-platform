package com.mega.center.system.model.dto.query;

import com.mega.common.core.web.model.BaseQueryDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 操作日志 查询对象
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "Log查询对象", description = "操作日志查询对象")
public class LogQueryDTO extends BaseQueryDTO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "模块标题")
    private String title;

    @ApiModelProperty(value = "业务类型")
    private String businessType;

    @ApiModelProperty(value = "方法名称")
    private String method;

    @ApiModelProperty(value = "操作类别（0其它 1后台用户 2手机端用户）")
    private Integer operatorType;

    @ApiModelProperty(value = "操作人员")
    private String operName;

    @ApiModelProperty(value = "主机地址")
    private String operIp;

    @ApiModelProperty(value = "操作状态（0正常 1异常）")
    private Integer status;
}
