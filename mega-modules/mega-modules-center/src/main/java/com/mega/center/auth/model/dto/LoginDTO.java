package com.mega.center.auth.model.dto;

import lombok.Data;

/**
 * 用户登录对象
 *
 * @author mega
 */
@Data
public class LoginDTO {
    /**
     * 用户名
     */
    private String username;

    /**
     * 用户密码
     */
    private String password;
}
