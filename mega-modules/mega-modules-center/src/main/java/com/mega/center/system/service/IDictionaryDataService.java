package com.mega.center.system.service;

import com.mega.center.system.model.entity.DictionaryData;
import com.mega.center.system.model.vo.DictionaryDataVO;
import com.mega.common.core.web.service.IMegaBaseService;

import java.util.List;

/**
 * 字典数据  服务
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
public interface IDictionaryDataService extends IMegaBaseService<DictionaryData> {

    /**
     * 新增字典数据
     *
     * @param dictionaryData 字典数据 实体类
     * @return
     */
    void create(DictionaryData dictionaryData);

    /**
     * 编辑字典数据
     *
     * @param dictionaryData 字典数据 实体类
     * @return
     */
    void update(DictionaryData dictionaryData);

    /**
     * 根据id删除字典数据
     *
     * @param id 主键值
     * @return
     */
    void remove(Integer id);

    /**
     * 根据字典编码查询字典数据:下拉
     *
     * @param dictCode
     * @return
     */
    List<DictionaryDataVO> listByDictCode(String dictCode);

    /**
     * 根据字典编码查询字典数据
     *
     * @param dictCode
     * @return
     */
    List<DictionaryData> listDetailByDictCode(String dictCode);

    /**
     * 刷新缓存
     */
    void refresh();

}
