package com.mega.center.system.controller;

import com.mega.center.system.model.entity.DictionaryData;
import com.mega.center.system.model.vo.DictionaryDataVO;
import com.mega.center.system.service.IDictionaryDataService;
import com.mega.common.core.domain.R;
import com.mega.common.core.web.controller.MageBaseController;
import com.mega.common.log.annotation.SysLog;
import com.mega.common.log.enums.BusinessType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 字典数据  前端控制器
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
@Slf4j
@RestController
@RequestMapping("/api/dictData")
@ApiModel(value = "字典数据 ", description = "字典数据 ")
@Api(tags = "字典数据 ")
public class DictionaryDataController extends MageBaseController<DictionaryData, IDictionaryDataService> {

    /**
     * 获取数据字典
     *
     * @return
     */
    @GetMapping("/{dictCode}")
    @ApiOperation(value = "获取数据字典")
    public R<List<DictionaryDataVO>> listByDictCode(@PathVariable String dictCode) {
        return R.ok(this.service.listByDictCode(dictCode));
    }

    /**
     * 根据字典编码查询数据详细数据
     *
     * @return
     */
    @GetMapping("/list")
    @ApiOperation(value = "根据字典编码查询数据详细数据")
    public R<List<DictionaryData>> listDetailByDictCode(@RequestParam String dictCode) {
        return R.ok(this.service.listDetailByDictCode(dictCode));
    }

    /**
     * 刷新缓存
     *
     * @return
     */
    @GetMapping("/refresh")
    @ApiOperation(value = "刷新缓存")
    public R refresh() {
        this.service.refresh();
        return R.ok();
    }

    /**
     * 新增字典数据
     *
     * @param dictionaryData 字典数据 实体类
     * @return R
     */
    @PostMapping
    @ApiOperation(value = "新增字典数据 ")
    @SysLog(title = "字典数据 ", businessType = BusinessType.INSERT)
    public R<Boolean> create(@RequestBody @Validated DictionaryData dictionaryData) {
        service.create(dictionaryData);
        return R.ok();
    }

    /**
     * 编辑字典数据
     *
     * @param dictionaryData 字典数据 实体类
     * @return R
     */
    @PutMapping
    @ApiOperation(value = "编辑字典数据 ")
    @SysLog(title = "字典数据 ", businessType = BusinessType.UPDATE)
    public R<Boolean> update(@RequestBody @Validated DictionaryData dictionaryData) {
        service.update(dictionaryData);
        return R.ok();
    }

    /**
     * 删除字典数据
     *
     * @param id 主键值
     * @return R
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除字典数据 ")
    @SysLog(title = "字典数据 ", businessType = BusinessType.DELETE)
    public R<Boolean> remove(@PathVariable Integer id) {
        service.remove(id);
        return R.ok();
    }

    /**
     * 根据主键查找
     *
     * @param id 主键值
     * @return R
     */
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "根据主键查找字典数据 ")
    public R<DictionaryData> getById(@PathVariable Integer id) {
        return R.ok(this.service.getById(id));
    }
}
