package com.mega.center.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mega.center.system.model.dto.query.LogQueryDTO;
import com.mega.center.system.model.entity.Log;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 操作日志 Mapper接口
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-24
 */
public interface LogMapper extends BaseMapper<Log> {

    /**
     * 批量插入
     *
     * @param log 操作日志实体类
     * @return
     */
    Integer createBatch(List<Log> log);

    /**
     * 分页查询
     *
     * @param page 分页参数,params 查询参数
     * @return
     */
    IPage<Log> listOfPage(IPage<Log> page, @Param("param") LogQueryDTO param);

}
