package com.mega.center.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mega.center.system.model.entity.RoleMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色菜单关联表 Mapper接口
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

    /**
     * 批量插入
     *
     * @param roleMenu 角色菜单关联表实体类
     * @return
     */
    Integer createBatch(List<RoleMenu> roleMenu);

    /**
     * 删除角色菜单权限
     *
     * @param roleCode 角色编码
     * @return
     */
    Integer deleteByRoleCode(@Param("roleCode") String roleCode);

}
