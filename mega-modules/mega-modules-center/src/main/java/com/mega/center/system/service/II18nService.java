package com.mega.center.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mega.center.system.model.dto.query.I18nQueryDTO;
import com.mega.center.system.model.entity.I18n;
import com.mega.center.system.model.vo.I18nVO;
import com.mega.common.core.web.service.IMegaBaseService;

import java.util.List;

/**
 * 国际化 服务
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
public interface II18nService extends IMegaBaseService<I18n> {

    /**
     * 新增国际化
     *
     * @param i18nVO 国际化实体类
     * @return
     */
    void create(I18nVO i18nVO);

    /**
     * 编辑国际化
     *
     * @param i18nVO 国际化实体类
     * @return
     */
    void update(I18nVO i18nVO);

    /**
     * 根据id删除国际化
     *
     * @param id 主键值
     * @return
     */
    void remove(Integer id);

    /**
     * 分页查询
     *
     * @param param 查询参数
     * @return
     */
    IPage<I18nVO> listOfPage(IPage<I18nVO> page, I18nQueryDTO param);

    /**
     * 获取全部国际化数据
     *
     * @return
     */
    List<I18nVO> listAll();

    /**
     * 刷新缓存
     */
    void refresh();
}
