package com.mega.center.system.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * 字典数据VO
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
@Data
@AllArgsConstructor
@ApiModel(value = "字典数据VO")
public class DictionaryDataVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "名称")
    private String label;

    @ApiModelProperty(value = "值")
    private String value;
}