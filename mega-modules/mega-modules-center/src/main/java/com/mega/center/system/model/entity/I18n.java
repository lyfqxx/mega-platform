package com.mega.center.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mega.common.core.web.model.entity.MegaBaseEntityNoDel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 国际化 实体
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_i18n")
@ApiModel(value = "I18n对象", description = "国际化")
public class I18n extends MegaBaseEntityNoDel<I18n> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "编码")
    @TableField("I18N_CODE")
    private String i18nCode;

    @ApiModelProperty(value = "类型：菜单-MENU,列-COLUMN,按钮-BUTTON,提示-TIPS,其他-OTHER")
    @TableField("TYPE")
    private String type;

    @ApiModelProperty(value = "描述")
    @TableField("REMARK")
    private String remark;

}