package com.mega.center.system.controller;

import com.mega.center.system.model.entity.Module;
import com.mega.system.api.vo.ModuleVO;
import com.mega.center.system.service.IModuleService;
import com.mega.common.core.domain.R;
import com.mega.common.core.web.controller.MageBaseController;
import com.mega.common.log.annotation.SysLog;
import com.mega.common.log.enums.BusinessType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统模块  前端控制器
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-24
 */
@Slf4j
@RestController
@RequestMapping("/api/module")
@ApiModel(value = "系统模块 ", description = "系统模块 ")
@Api(tags = "系统模块 ")
public class ModuleController extends MageBaseController<Module, IModuleService> {

    /**
     * 新增系统模块
     *
     * @param module 系统模块 实体类
     * @return R
     */
    @PostMapping
    @ApiOperation(value = "新增系统模块 ")
    @SysLog(title = "系统模块 ", businessType = BusinessType.INSERT)
    public R<Boolean> create(@RequestBody @Validated Module module) {
        service.create(module);
        return R.ok();
    }

    /**
     * 编辑系统模块
     *
     * @param module 系统模块 实体类
     * @return R
     */
    @PutMapping
    @ApiOperation(value = "编辑系统模块 ")
    @SysLog(title = "系统模块 ", businessType = BusinessType.UPDATE)
    public R<Boolean> update(@RequestBody @Validated Module module) {
        service.update(module);
        return R.ok();
    }

    /**
     * 删除系统模块
     *
     * @param id 主键值
     * @return R
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除系统模块 ")
    @SysLog(title = "系统模块 ", businessType = BusinessType.DELETE)
    public R<Boolean> remove(@PathVariable Integer id) {
        service.remove(id);
        return R.ok();
    }

    /**
     * 根据主键查找
     *
     * @param id 主键值
     * @return R
     */
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "根据主键查找系统模块 ")
    public R<Module> getById(@PathVariable Integer id) {
        return R.ok(this.service.getById(id));
    }

    /**
     * 查询子系统列表
     *
     * @return R
     */
    @GetMapping("/listSystemModule")
    @ApiOperation(value = "查询子系统列表 ")
    public R<List<ModuleVO>> listSystemModule() {
        return R.ok(this.service.listSystemModule());
    }

    /**
     * 修改模块状态
     *
     * @param status
     * @return
     */
    @PutMapping("/changeStatus")
    @ApiOperation(value = "修改模块状态")
    @SysLog(title = "系统模块", businessType = BusinessType.UPDATE)
    public R<Boolean> changeModuleStatus(@RequestParam Long id, @RequestParam String status) {
        service.changeModuleStatus(id, status);
        return R.ok();
    }
}
