package com.mega.center.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mega.center.system.model.dto.query.RoleQueryDTO;
import com.mega.center.system.model.entity.Role;
import com.mega.center.system.model.dto.ext.RoleExt;
import com.mega.common.core.web.service.IMegaBaseService;

/**
 * 角色表 服务
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
public interface IRoleService extends IMegaBaseService<Role> {

    /**
     * 新增角色表
     *
     * @param roleVo 角色表实体类
     * @return
     */
    void create(RoleExt roleVo);

    /**
     * 编辑角色表
     *
     * @param roleVo 角色表实体类
     * @return
     */
    void update(RoleExt roleVo);

    /**
     * 根据id删除角色表
     *
     * @param id 主键值
     * @return
     */
    void remove(Integer id);

    /**
     * 分页查询
     *
     * @param page 分页参数,params 查询参数
     * @return
     */
    IPage<Role> listOfPage(IPage<Role> page, RoleQueryDTO param);

}
