package com.mega.center.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mega.center.system.model.dto.query.RoleQueryDTO;
import com.mega.center.system.model.entity.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色表 Mapper接口
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 批量插入
     *
     * @param role 角色表实体类
     * @return
     */
    Integer createBatch(List<Role> role);

    /**
     * 分页查询
     *
     * @param page 分页参数,params 查询参数
     * @return
     */
    IPage<Role> listOfPage(IPage<Role> page, @Param("param") RoleQueryDTO param);

}
