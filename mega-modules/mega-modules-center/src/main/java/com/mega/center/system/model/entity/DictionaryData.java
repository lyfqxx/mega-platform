package com.mega.center.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mega.common.core.web.model.entity.MegaBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典数据  实体
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_dictionary_data")
@ApiModel(value = "DictionaryData对象", description = "字典数据 ")
public class DictionaryData extends MegaBaseEntity<DictionaryData> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "字典编码")
    @TableField("DICT_CODE")
    private String dictCode;

    @ApiModelProperty(value = "字典标签")
    @TableField("DATA_LABEL")
    private String dataLabel;

    @ApiModelProperty(value = "字典值")
    @TableField("DATA_VALUE")
    private String dataValue;

    @ApiModelProperty(value = "字典值扩展一")
    @TableField("DATA_VALUE_EXT1")
    private String dataValueExt1;

    @ApiModelProperty(value = "字典值扩展二")
    @TableField("DATA_VALUE_EXT2")
    private String dataValueExt2;

    @ApiModelProperty(value = "字典排序")
    @TableField("DATA_ORDER")
    private Integer dataOrder;

    @ApiModelProperty(value = "状态")
    @TableField("DATA_STATUS")
    private String dataStatus;

}