package com.mega.center.system.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mega.center.system.model.dto.query.LogQueryDTO;
import com.mega.center.system.model.entity.Log;
import com.mega.center.system.service.ILogService;
import com.mega.common.core.domain.R;
import com.mega.common.core.web.controller.MageBaseController;
import com.mega.system.api.vo.LogVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 操作日志 前端控制器
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-24
 */
@Slf4j
@RestController
@RequestMapping("/api/log")
@ApiModel(value = "操作日志", description = "操作日志")
@Api(tags = "操作日志")
public class LogController extends MageBaseController<Log, ILogService> {

    /**
     * 分页查询操作日志
     *
     * @param param 查询参数
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "分页查询操作日志集合")
    public R<IPage<Log>> listOfPage(LogQueryDTO param) {
        return R.ok(this.service.listOfPage(new Page<>(param.getPage(), param.getSize()), param));
    }

    /**
     * 新增操作日志
     *
     * @param logVO 操作日志实体类
     * @return R
     */
    @PostMapping
    @ApiOperation(value = "新增操作日志")
    public R<Boolean> create(@RequestBody @Validated LogVO logVO) {
        Log log = new Log();
        BeanUtil.copyProperties(logVO, log);
        service.create(log);
        return R.ok();
    }

    /**
     * 删除操作日志
     *
     * @param id 主键值
     * @return R
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除操作日志")
    public R<Boolean> remove(@PathVariable Integer id) {
        service.remove(id);
        return R.ok();
    }

    /**
     * 根据主键查找
     *
     * @param id 主键值
     * @return R
     */
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "根据主键查找操作日志")
    public R<Log> getById(@PathVariable Integer id) {
        return R.ok(this.service.getById(id));
    }
}
