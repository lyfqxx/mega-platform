package com.mega.center.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mega.center.system.model.dto.query.I18nQueryDTO;
import com.mega.center.system.model.entity.I18n;
import com.mega.center.system.model.vo.I18nVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 国际化 Mapper接口
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
public interface I18nMapper extends BaseMapper<I18n> {

    /**
     * 批量插入
     *
     * @param i18n 国际化实体类
     * @return
     */
    Integer createBatch(List<I18n> i18n);

    /**
     * 统计数量
     *
     * @param param 查询参数
     * @return
     */
    Long count(@Param("param") I18nQueryDTO param);

    /**
     * 分页查询
     *
     * @param param
     * @return
     */
    List<I18nVO> listOfPage(@Param("param") I18nQueryDTO param);

}
