package com.mega.center.system.model.vo;

import com.mega.center.system.model.entity.User;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "UserVO对象", description = "用户VO")
public class UserDetailVO extends User {

    private static final long serialVersionUID = 1L;


}
