package com.mega.center.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mega.center.system.model.dto.query.I18nQueryDTO;
import com.mega.center.system.model.entity.I18n;
import com.mega.center.system.model.vo.I18nVO;
import com.mega.center.system.service.II18nService;
import com.mega.common.core.domain.R;
import com.mega.common.core.web.controller.MageBaseController;
import com.mega.common.log.annotation.SysLog;
import com.mega.common.log.enums.BusinessType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 国际化 前端控制器
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
@Slf4j
@RestController
@RequestMapping("/api/i18n")
@ApiModel(value = "国际化", description = "国际化")
@Api(tags = "国际化")
public class I18nController extends MageBaseController<I18n, II18nService> {

    /**
     * 分页查询国际化
     *
     * @param param 查询参数
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "分页查询国际化集合")
    public R<IPage<I18nVO>> listOfPage(I18nQueryDTO param) {
        return R.ok(this.service.listOfPage(new Page<>(param.getPage(), param.getSize()), param));
    }

    /**
     * 刷新缓存
     *
     * @return
     */
    @GetMapping("/refresh")
    @ApiOperation(value = "刷新缓存")
    public R refresh() {
        this.service.refresh();
        return R.ok();
    }

    /**
     * 获取所有国际化数据
     * @return
     */
    @GetMapping("/list")
    @ApiOperation(value = "获取所有国际化数据")
    public R<List<I18nVO>> listAll() {
        return R.ok(this.service.listAll());
    }

    /**
     * 新增国际化
     *
     * @param i18nVO 国际化实体类
     * @return R
     */
    @PostMapping
    @ApiOperation(value = "新增国际化")
    @SysLog(title = "国际化", businessType = BusinessType.INSERT)
    public R<Boolean> create(@RequestBody @Validated I18nVO i18nVO) {
        service.create(i18nVO);
        return R.ok();
    }

    /**
     * 编辑国际化
     *
     * @param i18nVO 国际化实体类
     * @return R
     */
    @PutMapping
    @ApiOperation(value = "编辑国际化")
    @SysLog(title = "国际化", businessType = BusinessType.UPDATE)
    public R<Boolean> update(@RequestBody @Validated I18nVO i18nVO) {
        service.update(i18nVO);
        return R.ok();
    }

    /**
     * 删除国际化
     *
     * @param id 主键值
     * @return R
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除国际化")
    @SysLog(title = "国际化", businessType = BusinessType.DELETE)
    public R<Boolean> remove(@PathVariable Integer id) {
        service.remove(id);
        return R.ok();
    }

    /**
     * 根据主键查找
     *
     * @param id 主键值
     * @return R
     */
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "根据主键查找国际化")
    public R<I18n> getById(@PathVariable Integer id) {
        return R.ok(this.service.getById(id));
    }
}
