package com.mega.center.system.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 国际化数据VO对象
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-29
 */
@Data
@ApiModel(value = "国际化数据VO对象")
public class I18nDataVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    private Long id;

    @ApiModelProperty(value = "编码")
    private String i18nCode;

    @ApiModelProperty(value = "值")
    private String dataValue;

    @ApiModelProperty(value = "所属语言编码")
    private String languageCode;

    @ApiModelProperty(value = "所属语言名称")
    private String languageName;

}