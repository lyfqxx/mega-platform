package com.mega.center.system.model.dto.query;

import com.mega.common.core.web.model.BaseQueryDTO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典  查询对象
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "Dictionary查询对象", description = "字典 查询对象")
public class DictionaryQueryDTO extends BaseQueryDTO {

    private static final long serialVersionUID = 1L;

}
