package com.mega.center.system.service;

import com.mega.center.system.model.entity.Menu;
import com.mega.center.system.model.vo.MenuTreeVO;
import com.mega.center.system.model.vo.UserModuleMenuVO;
import com.mega.common.core.web.model.TreeNodeSelect;
import com.mega.common.core.web.service.IMegaBaseService;
import com.mega.system.api.vo.ModuleVO;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 菜单表 服务
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
public interface IMenuService extends IMegaBaseService<Menu> {

    /**
     * 新增菜单表
     *
     * @param menu 菜单表实体类
     * @return
     */
    void create(Menu menu);

    /**
     * 编辑菜单表
     *
     * @param menu 菜单表实体类
     * @return
     */
    void update(Menu menu);

    /**
     * 根据id删除菜单表
     *
     * @param id 主键值
     * @return
     */
    void remove(Integer id);

    /**
     * 获取单个系统菜单:选择组件
     *
     * @return
     */
    List<TreeNodeSelect> listSystemMenuForSelect(String systemCode);

    /**
     * 获取单个系统菜单
     *
     * @return
     */
    List<MenuTreeVO> listSystemMenu(String systemCode);

    /**
     * 获取授权菜单数据
     *
     * @return
     */
    List<ModuleVO> listModuleMenu();

    /**
     * 获取用户菜单
     *
     * @param userName
     * @return
     */
    UserModuleMenuVO getUserModuleAndMenuInfo(String userName);

    /**
     * 按模块获取用户菜单权限
     *
     * @param userName
     * @param systemCode
     * @return
     */
    UserModuleMenuVO getUserMenuInfoByModule(String userName, String systemCode);

    /**
     * 根据编码获取菜单信息
     *
     * @param code
     * @return
     */
    Menu getMenuByCode(String code);

    /**
     * 根据系统模块获取用户菜单
     *
     * @param userName
     * @param userModuleCodes
     * @return
     */
    Map<String, List<MenuTreeVO>> getUserMenuByModules(String userName, Set<String> userModuleCodes);

    /**
     * 根据系统模块获取用户权限
     *
     * @param userName
     * @param userModuleCodes
     * @return
     */
    Map<String, Set<String>> getUserPermissionByModules(String userName, Set<String> userModuleCodes);
}
