package com.mega.center.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mega.center.system.model.entity.I18nData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 国际化数据 Mapper接口
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
public interface I18nDataMapper extends BaseMapper<I18nData> {

    /**
     * 批量插入
     *
     * @param i18nData 国际化数据实体类
     * @return
     */
    Integer createBatch(List<I18nData> i18nData);

    /**
     * 根据编码删除
     *
     * @param i18nCode
     * @return
     */
    Integer deleteByCode(@Param("i18nCode") String i18nCode);

}
