package com.mega.center.system.controller;

import com.mega.center.system.model.entity.Dictionary;
import com.mega.center.system.model.vo.DictionaryTreeVO;
import com.mega.center.system.service.IDictionaryService;
import com.mega.common.core.domain.R;
import com.mega.common.core.web.controller.MageBaseController;
import com.mega.common.log.annotation.SysLog;
import com.mega.common.log.enums.BusinessType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 字典  前端控制器
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
@Slf4j
@RestController
@RequestMapping("/api/dict")
@ApiModel(value = "字典", description = "字典")
@Api(tags = "字典 ")
public class DictionaryController extends MageBaseController<Dictionary, IDictionaryService> {

    /**
     * 获取树形字典
     *
     * @return
     */
    @GetMapping("/tree")
    @ApiOperation(value = "获取树形字典")
    public R<List<DictionaryTreeVO>> listDictForTree() {
        return R.ok(this.service.listDictForTree());
    }

    /**
     * 获取子节点数据
     *
     * @return
     */
    @GetMapping("/listChild")
    @ApiOperation(value = "获取子节点数据")
    public R<List<Dictionary>> listChild(@RequestParam(required = false, defaultValue = "0") String parentCode) {
        return R.ok(this.service.listChild(parentCode));
    }

    /**
     * 新增字典
     *
     * @param dictionary 字典 实体类
     * @return R
     */
    @PostMapping
    @ApiOperation(value = "新增字典 ")
    @SysLog(title = "字典 ", businessType = BusinessType.INSERT)
    public R<Boolean> create(@RequestBody @Validated Dictionary dictionary) {
        service.create(dictionary);
        return R.ok();
    }

    /**
     * 编辑字典
     *
     * @param dictionary 字典 实体类
     * @return R
     */
    @PutMapping
    @ApiOperation(value = "编辑字典 ")
    @SysLog(title = "字典 ", businessType = BusinessType.UPDATE)
    public R<Boolean> update(@RequestBody @Validated Dictionary dictionary) {
        service.update(dictionary);
        return R.ok();
    }

    /**
     * 删除字典
     *
     * @param id 主键值
     * @return R
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除字典 ")
    @SysLog(title = "字典 ", businessType = BusinessType.DELETE)
    public R<Boolean> remove(@PathVariable Integer id) {
        service.remove(id);
        return R.ok();
    }

    /**
     * 根据主键查找
     *
     * @param id 主键值
     * @return R
     */
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "根据主键查找字典 ")
    public R<Dictionary> getById(@PathVariable Integer id) {
        return R.ok(this.service.getById(id));
    }
}
