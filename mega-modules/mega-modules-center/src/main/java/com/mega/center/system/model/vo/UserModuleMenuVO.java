package com.mega.center.system.model.vo;

import com.mega.system.api.vo.ModuleVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@ApiModel(value = "用户模块菜单VO")
public class UserModuleMenuVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户模块列表")
    private List<ModuleVO> modules;

    @ApiModelProperty(value = "用户模块菜单列表")
    private Map<String, List<MenuTreeVO>> menus;

    @ApiModelProperty(value = "用户模块权限列表")
    private Map<String, Set<String>> permissions;
}
