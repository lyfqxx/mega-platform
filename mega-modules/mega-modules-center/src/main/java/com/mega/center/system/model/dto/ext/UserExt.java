package com.mega.center.system.model.dto.ext;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mega.center.system.model.entity.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "用户扩展对象")
public class UserExt extends User {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "角色编码集合")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<String> rolesCodes;
}
