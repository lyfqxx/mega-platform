package com.mega.center.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mega.center.system.model.entity.Module;
import com.mega.system.api.vo.ModuleVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统模块  Mapper接口
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-24
 */
public interface ModuleMapper extends BaseMapper<Module> {

    /**
     * 批量插入
     *
     * @param module 系统模块 实体类
     * @return
     */
    Integer createBatch(List<Module> module);

    /**
     * 查询子系统列表：层级为1的数据
     *
     * @return
     */
    List<ModuleVO> listSystemModule(@Param("status") String status);

    /**
     * 获取用户所拥有权限的模块
     *
     * @param userName
     * @return
     */
    List<ModuleVO> listUserModule(@Param("userName") String userName);
}
