package com.mega.center.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mega.center.system.model.entity.Dictionary;
import com.mega.center.system.model.vo.DictionaryTreeVO;

import java.util.List;

/**
 * 字典  Mapper接口
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
public interface DictionaryMapper extends BaseMapper<Dictionary> {

    /**
     * 批量插入
     *
     * @param dictionary 字典 实体类
     * @return
     */
    Integer createBatch(List<Dictionary> dictionary);

    /**
     * 获取数据字典
     *
     * @return
     */
    List<DictionaryTreeVO> listDictForTree();

}
