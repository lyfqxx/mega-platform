package com.mega.center.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mega.center.system.model.dto.query.UserQueryDTO;
import com.mega.center.system.model.entity.User;
import com.mega.system.api.vo.UserInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户表 Mapper接口
 * SqlSessionFactory
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 批量插入
     *
     * @param user 用户表实体类
     * @return
     */
    Integer createBatch(List<User> user);

    /**
     * 分页查询
     *
     * @param page 分页参数,params 查询参数
     * @return
     */
    IPage<User> listOfPage(IPage<User> page, @Param("param") UserQueryDTO param);

    /**
     * 根据用户名获取用户信息
     *
     * @param userName
     * @return
     */
    UserInfo getUserInfoByUserName(String userName);

}
