package com.mega.center.system.model.dto.query;

import com.mega.common.core.web.model.BaseQueryDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 国际化 查询对象
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "I18n查询对象", description = "国际化查询对象")
public class I18nQueryDTO extends BaseQueryDTO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "编码")
    private String i18nCode;

    @ApiModelProperty(value = "类型：菜单-MENU,列-COLUMN,按钮-BUTTON,提示-TIPS,其他-OTHER")
    private String type;

    @ApiModelProperty(value = "描述")
    private String remark;
}
