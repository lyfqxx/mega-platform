package com.mega.center.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mega.center.system.mapper.RoleMapper;
import com.mega.center.system.mapper.RoleMenuMapper;
import com.mega.center.system.model.dto.query.RoleQueryDTO;
import com.mega.center.system.model.entity.Role;
import com.mega.center.system.model.entity.RoleMenu;
import com.mega.center.system.model.dto.ext.RoleExt;
import com.mega.center.system.service.IRoleService;
import com.mega.common.core.enums.CommonMsgEnum;
import com.mega.common.core.exception.BusinessException;
import com.mega.common.core.web.service.MegaBaseServiceImpl;
import com.mega.common.redis.utils.CodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色表 服务实现
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
@Service
public class RoleServiceImpl extends MegaBaseServiceImpl<RoleMapper, Role> implements IRoleService {

    @Autowired
    private RoleMenuMapper roleMenuMapper;

    /**
     * 新增角色表
     *
     * @param roleVo 角色表实体类
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(RoleExt roleVo) {
        Integer nameCount = this.count(Wrappers.<Role>lambdaQuery()
                .eq(Role::getRoleName, roleVo.getRoleName()));
        if (nameCount != null && nameCount > 0) {
            throw new BusinessException(CommonMsgEnum.NAME_REPEAT, roleVo.getRoleName());
        }
        roleVo.setRoleCode(CodeUtils.getDateCode(new Role()));
        //保存角色信息
        this.save(roleVo);

        //保存角色菜单数据
        if (CollectionUtil.isNotEmpty(roleVo.getMenuCodes())) {
            List<RoleMenu> roleMenuList = new ArrayList<>();
            roleVo.getMenuCodes().forEach(v -> {
                roleMenuList.add(new RoleMenu(roleVo.getRoleCode(), v));
            });
            roleMenuMapper.createBatch(roleMenuList);
        }
    }

    /**
     * 编辑角色表
     *
     * @param roleVo 角色表实体类
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(RoleExt roleVo) {
        //判断数据是否为空
        Role queryEntity = this.getById(roleVo.getId());
        if (queryEntity == null) {
            throw new BusinessException(CommonMsgEnum.DATE_NOT_EXIST);
        }
        //查询修改后名称是否与原先一致
        if (!roleVo.getRoleName().equals(queryEntity.getRoleName())) {
            //判断名称是否重复
            Integer nameCount = this.count(Wrappers.<Role>lambdaQuery()
                    .eq(Role::getRoleName, roleVo.getRoleName()));
            if (nameCount != null && nameCount > 0) {
                throw new BusinessException(CommonMsgEnum.NAME_REPEAT, roleVo.getRoleName());
            }
        }
        roleVo.setRoleCode(queryEntity.getRoleCode());
        //更新数据
        this.updateById(roleVo);

        //删除角色菜单
        roleMenuMapper.deleteByRoleCode(roleVo.getRoleCode());
        //保存角色菜单数据
        if (CollectionUtil.isNotEmpty(roleVo.getMenuCodes())) {
            List<RoleMenu> roleMenuList = new ArrayList<>();
            roleVo.getMenuCodes().forEach(v -> {
                roleMenuList.add(new RoleMenu(roleVo.getRoleCode(), v));
            });
            roleMenuMapper.createBatch(roleMenuList);
        }
    }

    /**
     * 根据id删除角色表
     *
     * @param id 主键值
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void remove(Integer id) {
        this.removeById(id);
    }

    /**
     * 分页查询
     *
     * @param page  分页参数
     * @param param 查询参数
     * @return
     */
    @Override
    public IPage<Role> listOfPage(IPage<Role> page, RoleQueryDTO param) {
        return this.baseMapper.listOfPage(page, param);
    }
}
