package com.mega.center.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mega.common.core.web.model.entity.MegaBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户表 实体
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_user")
@ApiModel(value = "User对象", description = "用户表")
public class User extends MegaBaseEntity<User> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户名")
    @TableField("USER_NAME")
    private String userName;

    @ApiModelProperty(value = "密码")
    @TableField("PASSWORD")
    @JsonIgnore
    private String password;

    @ApiModelProperty(value = "关联员工工号")
    @TableField("EMPLOYEE_NO")
    private String employeeNo;

    @ApiModelProperty(value = "姓名")
    @TableField("NAME")
    private String name;

    @ApiModelProperty(value = "头像")
    @TableField("AVATAR")
    private String avatar;

    @ApiModelProperty(value = "性别:M男 F女")
    @TableField("SEX")
    private String sex;

    @ApiModelProperty(value = "状态")
    @TableField("STATUS")
    private String status;

    @ApiModelProperty(value = "手机号码")
    @TableField("MOBILE_PHONE")
    private String mobilePhone;

    @ApiModelProperty(value = "邮箱")
    @TableField("EMAIL")
    private String email;

    @ApiModelProperty(value = "办公电话")
    @TableField("WORK_PHONE")
    private String workPhone;

    @ApiModelProperty(value = "公司编码")
    @TableField("COMPANY_CODE")
    private String companyCode;

    @ApiModelProperty(value = "部门编码")
    @TableField("DEPARTMENT_CODE")
    private String departmentCode;

    @ApiModelProperty(value = "岗位代码")
    @TableField("POSITION_CODE")
    private String positionCode;

}