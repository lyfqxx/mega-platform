package com.mega.center.system.model.dto.query;

import com.mega.common.core.web.model.BaseQueryDTO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色菜单关联表 查询对象
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "RoleMenu查询对象", description = "角色菜单关联表查询对象")
public class RoleMenuQueryDTO extends BaseQueryDTO {

    private static final long serialVersionUID = 1L;

}
