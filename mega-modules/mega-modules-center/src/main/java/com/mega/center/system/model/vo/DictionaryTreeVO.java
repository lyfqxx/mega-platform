package com.mega.center.system.model.vo;

import com.mega.common.core.web.model.TreeNode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "字典树VO对象")
public class DictionaryTreeVO extends TreeNode<String, DictionaryTreeVO> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "菜单ID")
    private Long id;

    @ApiModelProperty(value = "字典编码 英文标识")
    private String dictCode;

    @ApiModelProperty(value = "字典名称 中文标识")
    private String dictName;

    @ApiModelProperty(value = "状态")
    private String dictStatus;

    @ApiModelProperty(value = "字典类别 S：系统字典，U：用户字典")
    private String dictCategory;

    @ApiModelProperty(value = "父层级编码")
    private String parentDictCode;

    @ApiModelProperty(value = "显示顺序")
    private Integer dictSort;

    @ApiModelProperty(value = "层级类型 M目录 D字典")
    private String type;
}
