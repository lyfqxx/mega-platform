package com.mega.center.system.model.dto.ext;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mega.center.system.model.entity.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "角色扩展对象")
public class RoleExt extends Role {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "菜单编码集合")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<String> menuCodes;
}
