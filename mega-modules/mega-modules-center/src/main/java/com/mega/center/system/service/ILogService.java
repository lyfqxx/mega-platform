package com.mega.center.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mega.center.system.model.dto.query.LogQueryDTO;
import com.mega.center.system.model.entity.Log;
import com.mega.common.core.web.service.IMegaBaseService;

/**
 * 操作日志 服务
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-24
 */
public interface ILogService extends IMegaBaseService<Log> {

    /**
     * 新增操作日志
     *
     * @param log 操作日志实体类
     * @return
     */
    void create(Log log);

    /**
     * 根据id删除操作日志
     *
     * @param id 主键值
     * @return
     */
    void remove(Integer id);

    /**
     * 分页查询
     *
     * @param page 分页参数,params 查询参数
     * @return
     */
    IPage<Log> listOfPage(IPage<Log> page, LogQueryDTO param);

}
