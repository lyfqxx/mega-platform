package com.mega.center.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mega.center.system.model.entity.DictionaryData;

import java.util.List;

/**
 * 字典数据  Mapper接口
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
public interface DictionaryDataMapper extends BaseMapper<DictionaryData> {

    /**
     * 批量插入
     *
     * @param dictionaryData 字典数据 实体类
     * @return
     */
    Integer createBatch(List<DictionaryData> dictionaryData);


    /**
     * 获取有效的字典数据
     *
     * @return
     */
    List<DictionaryData> listValidDictionaryData();

}
