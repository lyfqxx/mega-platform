package com.mega.center.system.enums;

/**
 * 系统管理相关枚举类
 */
public interface SysEnums {

    /**
     * 字典类型
     */
    enum DictionaryType {

        MENU("M", "目录"),
        DICTIONARY("D", "字典");

        private String code;
        private String name;

        DictionaryType(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }
    }

    /**
     * 字典类型
     */
    enum DictionaryCategory {

        SYSTEM("S", "系统字典"),
        USER("U", "用户字典");

        private String code;
        private String name;

        DictionaryCategory(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }
    }
}
