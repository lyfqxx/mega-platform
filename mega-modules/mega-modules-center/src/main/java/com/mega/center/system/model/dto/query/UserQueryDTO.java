package com.mega.center.system.model.dto.query;

import com.mega.common.core.web.model.BaseQueryDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户表 查询对象
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "User查询对象", description = "用户表查询对象")
public class UserQueryDTO extends BaseQueryDTO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "关联员工工号")
    private String employeeNo;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "性别:M男 F女")
    private String sex;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "手机号码")
    private String mobilePhone;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "办公电话")
    private String workPhone;

    @ApiModelProperty(value = "公司编码")
    private String companyCode;

    @ApiModelProperty(value = "部门编码")
    private String departmentCode;

    @ApiModelProperty(value = "岗位代码")
    private String positionCode;
}
