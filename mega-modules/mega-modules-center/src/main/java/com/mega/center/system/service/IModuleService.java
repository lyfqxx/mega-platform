package com.mega.center.system.service;

import com.mega.center.system.model.entity.Module;
import com.mega.system.api.vo.ModuleVO;
import com.mega.common.core.web.service.IMegaBaseService;

import java.util.List;

/**
 * 系统模块  服务
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-24
 */
public interface IModuleService extends IMegaBaseService<Module> {

    /**
     * 新增系统模块
     *
     * @param module 系统模块 实体类
     * @return
     */
    void create(Module module);

    /**
     * 编辑系统模块
     *
     * @param module 系统模块 实体类
     * @return
     */
    void update(Module module);

    /**
     * 根据id删除系统模块
     *
     * @param id 主键值
     * @return
     */
    void remove(Integer id);

    /**
     * 根据编码获取模块信息
     *
     * @param code
     * @return
     */
    Module getModuleByCode(String code);

    /**
     * 查询子系统列表：层级为1的数据
     *
     * @return
     */
    List<ModuleVO> listSystemModule();

    /**
     * 修改模块状态
     *
     * @param id
     * @param status
     */
    void changeModuleStatus(Long id, String status);

    /**
     * 获取用户拥有的模块
     *
     * @param userName
     * @return
     */
    List<ModuleVO> listUserModule(String userName);

}
