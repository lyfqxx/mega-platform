package com.mega.center.system.model.dto.query;

import com.mega.common.core.web.model.BaseQueryDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统模块  查询对象
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "Module查询对象", description = "系统模块 查询对象")
public class ModuleQueryDTO extends BaseQueryDTO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态：正常(NORMAL) 禁用(DISABLED)")
    private String status;
}
