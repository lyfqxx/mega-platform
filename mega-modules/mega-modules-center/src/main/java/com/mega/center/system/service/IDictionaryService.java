package com.mega.center.system.service;

import com.mega.center.system.model.entity.Dictionary;
import com.mega.center.system.model.vo.DictionaryTreeVO;
import com.mega.common.core.web.service.IMegaBaseService;

import java.util.List;

/**
 * 字典  服务
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-28
 */
public interface IDictionaryService extends IMegaBaseService<Dictionary> {

    /**
     * 新增字典
     *
     * @param dictionary 字典 实体类
     * @return
     */
    void create(Dictionary dictionary);

    /**
     * 编辑字典
     *
     * @param dictionary 字典 实体类
     * @return
     */
    void update(Dictionary dictionary);

    /**
     * 根据编码获取字典
     *
     * @param dictCode
     * @return
     */
    Dictionary getDictionaryByCode(String dictCode);

    /**
     * 根据id删除字典
     *
     * @param id 主键值
     * @return
     */
    void remove(Integer id);

    /**
     * 获取树形字典
     */
    List<DictionaryTreeVO> listDictForTree();

    /**
     * 获取子节点
     *
     * @return
     */
    List<Dictionary> listChild(String parentCode);

}
