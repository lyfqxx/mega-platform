package com.mega.center.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mega.center.system.model.entity.UserRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户角色关系表 Mapper接口
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

    /**
     * 批量插入
     *
     * @param userRole 用户角色关系表实体类
     * @return
     */
    Integer createBatch(List<UserRole> userRole);

    /**
     * 删除用户角色
     *
     * @param userName
     * @return
     */
    Integer deleteByUserName(@Param("userName") String userName);

}
