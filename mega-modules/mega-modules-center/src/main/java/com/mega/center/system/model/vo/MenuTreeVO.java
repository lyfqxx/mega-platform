package com.mega.center.system.model.vo;

import com.mega.common.core.web.model.TreeNode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "菜单树VO对象")
public class MenuTreeVO extends TreeNode<String, MenuTreeVO> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "菜单ID")
    private Long id;

    @ApiModelProperty(value = "菜单编码")
    private String menuCode;

    @ApiModelProperty(value = "菜单名称")
    private String menuName;

    @ApiModelProperty(value = "父菜单编码")
    private String parentMenuCode;

    @ApiModelProperty(value = "父菜单编码集")
    private String parentMenuCodes;

    @ApiModelProperty(value = "显示顺序")
    private String menuSort;

    @ApiModelProperty(value = "路由地址")
    private String menuPath;

    @ApiModelProperty(value = "组件")
    private String component;

    @ApiModelProperty(value = "是否外链")
    private String isFrame;

    @ApiModelProperty(value = "菜单类型 M目录 C菜单 F按钮")
    private String menuType;

    @ApiModelProperty(value = "是否显示")
    private String visible;

    @ApiModelProperty(value = "权限标识")
    private String perms;

    @ApiModelProperty(value = "菜单图标")
    private String icon;

    @ApiModelProperty(value = "所属系统编码")
    private String systemCode;

    @ApiModelProperty(value = "层级")
    private Integer treeLevel;
}
