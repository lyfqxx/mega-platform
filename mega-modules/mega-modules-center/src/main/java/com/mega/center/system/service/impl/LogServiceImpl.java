package com.mega.center.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mega.center.system.mapper.LogMapper;
import com.mega.center.system.model.dto.query.LogQueryDTO;
import com.mega.center.system.model.entity.Log;
import com.mega.center.system.service.ILogService;
import com.mega.common.core.web.service.MegaBaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 操作日志 服务实现
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-24
 */
@Service
public class LogServiceImpl extends MegaBaseServiceImpl<LogMapper, Log> implements ILogService {

    /**
     * 新增操作日志
     *
     * @param log 操作日志实体类
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(Log log) {
        this.save(log);
    }

    /**
     * 根据id删除操作日志
     *
     * @param id 主键值
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void remove(Integer id) {
        this.removeById(id);
    }

    /**
     * 分页查询
     *
     * @param page  分页参数
     * @param param 查询参数
     * @return
     */
    @Override
    public IPage<Log> listOfPage(IPage<Log> page, LogQueryDTO param) {
        return this.baseMapper.listOfPage(page, param);
    }

}
