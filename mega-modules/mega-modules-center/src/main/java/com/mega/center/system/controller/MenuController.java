package com.mega.center.system.controller;

import com.mega.center.system.model.entity.Menu;
import com.mega.center.system.model.vo.MenuTreeVO;
import com.mega.center.system.model.vo.UserModuleMenuVO;
import com.mega.center.system.service.IMenuService;
import com.mega.common.core.domain.R;
import com.mega.common.core.web.controller.MageBaseController;
import com.mega.common.log.annotation.SysLog;
import com.mega.common.log.enums.BusinessType;
import com.mega.common.security.utils.SecurityUtils;
import com.mega.system.api.vo.ModuleVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 菜单表 前端控制器
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
@Slf4j
@RestController
@RequestMapping("/api/menu")
@ApiModel(value = "菜单表", description = "菜单表")
@Api(tags = "菜单表")
public class MenuController extends MageBaseController<Menu, IMenuService> {

    /**
     * 获取单个系统菜单
     *
     * @return
     */
    @GetMapping("/listSystemMenu/{systemCode}")
    @ApiOperation(value = "获取单个系统菜单")
    public R<List<MenuTreeVO>> listSystemMenu(@PathVariable String systemCode) {
        return R.ok(this.service.listSystemMenu(systemCode));
    }

    /**
     * 获取授权菜单数据
     *
     * @return
     */
    @GetMapping("/listModuleMenu")
    @ApiOperation(value = "获取授权菜单数据")
    public R<List<ModuleVO>> listModuleMenu() {
        return R.ok(this.service.listModuleMenu());
    }

    /**
     * 获取用户模块/菜单/权限
     *
     * @return
     */
    @GetMapping("/userMenu")
    @ApiOperation(value = "获取用户模块/菜单/权限")
    public R<UserModuleMenuVO> getUserModuleAndMenuInfo() {
        return R.ok(this.service.getUserModuleAndMenuInfo(SecurityUtils.getUsername()));
    }

    /**
     * 按模块获取用户菜单/权限
     *
     * @param systemCode
     * @return
     */
    @GetMapping("/userMenu/{systemCode}")
    @ApiOperation(value = "按模块获取用户菜单/权限")
    public R<UserModuleMenuVO> getUserMenuInfoByModule(@PathVariable String systemCode) {
        return R.ok(this.service.getUserMenuInfoByModule(SecurityUtils.getUsername(), systemCode));
    }

    /**
     * 新增菜单表
     *
     * @param menu 菜单表实体类
     * @return R
     */
    @PostMapping
    @ApiOperation(value = "新增菜单表")
    @SysLog(title = "菜单表", businessType = BusinessType.INSERT)
    public R<Boolean> create(@RequestBody @Validated Menu menu) {
        service.create(menu);
        return R.ok();
    }

    /**
     * 编辑菜单表
     *
     * @param menu 菜单表实体类
     * @return R
     */
    @PutMapping
    @ApiOperation(value = "编辑菜单表")
    @SysLog(title = "菜单表", businessType = BusinessType.UPDATE)
    public R<Boolean> update(@RequestBody @Validated Menu menu) {
        service.update(menu);
        return R.ok();
    }

    /**
     * 删除菜单表
     *
     * @param id 主键值
     * @return R
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除菜单表")
    @SysLog(title = "菜单表", businessType = BusinessType.DELETE)
    public R<Boolean> remove(@PathVariable Integer id) {
        service.remove(id);
        return R.ok();
    }

    /**
     * 根据主键查找
     *
     * @param id 主键值
     * @return R
     */
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "根据主键查找菜单表")
    public R<Menu> getById(@PathVariable Integer id) {
        return R.ok(this.service.getById(id));
    }
}
