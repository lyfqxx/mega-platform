package com.mega.center.system.model.dto.query;

import com.mega.common.core.web.model.BaseQueryDTO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户角色关系表 查询对象
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "UserRole查询对象", description = "用户角色关系表查询对象")
public class UserRoleQueryDTO extends BaseQueryDTO {

    private static final long serialVersionUID = 1L;

}
