package com.mega.center.system.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 国际化VO
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-29
 */
@Data
@ApiModel(value = "国际化VO")
public class I18nVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    private Long id;

    @ApiModelProperty(value = "编码")
    private String i18nCode;

    @ApiModelProperty(value = "类型：菜单-MENU,列-COLUMN,按钮-BUTTON,提示-TIPS,其他-OTHER")
    private String type;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "数据明细")
    private List<I18nDataVO> items;
}