package com.mega.center.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mega.center.system.model.dto.ext.UserExt;
import com.mega.center.system.model.dto.query.UserQueryDTO;
import com.mega.center.system.model.entity.User;
import com.mega.center.system.model.vo.UserDetailVO;
import com.mega.common.core.web.service.IMegaBaseService;
import com.mega.system.api.vo.UserAuthInfo;
import com.mega.system.api.vo.UserInfo;

import java.util.Set;

/**
 * 用户表 服务
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-23
 */
public interface IUserService extends IMegaBaseService<User> {

    /**
     * 新增用户表
     *
     * @param user 用户表实体类
     * @return
     */
    void create(UserExt user);

    /**
     * 编辑用户表
     *
     * @param user 用户表实体类
     * @return
     */
    void update(UserExt user);

    /**
     * 根据id删除用户表
     *
     * @param id 主键值
     * @return
     */
    void remove(Long id);

    /**
     * 分页查询
     *
     * @param page 分页参数,params 查询参数
     * @return
     */
    IPage<User> listOfPage(IPage<User> page, UserQueryDTO param);


    /**
     * 根据用户名获取用户信息
     *
     * @param userName
     * @return
     */
    UserInfo getUserInfoByUserName(String userName);

    /**
     * 修改密码
     *
     * @param userId
     * @param oldPwd
     * @param newPwd
     */
    void changePwd(Long userId, String oldPwd, String newPwd);

    /**
     * 重置密码
     *
     * @param userId
     */
    void resetPwd(Long userId);

    /**
     * 修改密码
     *
     * @param userId
     * @param status
     */
    void changeUserStatus(Long userId, String status);

    /**
     * 获取用户详细信息
     *
     * @param userId
     * @return
     */
    UserDetailVO getUserDetailInfo(Long userId);

    /**
     * 获取用户角色权限
     *
     * @param userName
     * @return
     */
    Set<String> getUserRolePermission(String userName);

    /**
     * 获取用户权限信息
     *
     * @param userName
     * @return
     */
    UserAuthInfo getUserAuthInfoForLogin(String userName);
}
