package com.mega.common.security.utils;

import cn.hutool.core.convert.Convert;
import com.mega.common.core.constant.CacheConstants;
import com.mega.common.core.constant.UserConstants;
import com.mega.common.core.utils.ServletUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 权限获取工具类
 *
 * @author mega
 */
public class SecurityUtils {
    /**
     * 获取用户
     */
    public static String getUsername() {
        return ServletUtils.getRequest().getHeader(CacheConstants.DETAILS_USERNAME);
    }

    /**
     * 获取用户ID
     */
    public static Long getUserId() {
        return Convert.toLong(ServletUtils.getRequest().getHeader(CacheConstants.DETAILS_USER_ID));
    }

    /**
     * 是否为管理员
     *
     * @param userName 用户名
     * @return 结果
     */
    public static boolean isAdmin(String userName) {
        return userName != null && UserConstants.ADMIN_NAME.equals(userName);
    }

    /**
     * 生成BCryptPasswordEncoder密码
     *
     * @param password 密码
     * @return 加密字符串
     */
    public static String encryptPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    /**
     * 判断密码是否相同
     *
     * @param rawPassword     真实密码
     * @param encodedPassword 加密后字符
     * @return 结果
     */
    public static boolean matchesPassword(String rawPassword, String encodedPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }
}
