package com.mega.common.security.aspect;

import com.mega.common.core.constant.UserConstants;
import com.mega.common.core.exception.PreAuthorizeException;
import com.mega.common.security.annotation.PreAuthorize;
import com.mega.common.security.service.TokenService;
import com.mega.system.api.vo.LoginUser;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.PatternMatchUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.Collection;

/**
 * 自定义权限实现
 *
 * @author mega
 */
@Aspect
@Component
public class PreAuthorizeAspect {

    @Autowired
    private TokenService tokenService;

    @Around("@annotation(com.mega.common.security.annotation.PreAuthorize)")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        Signature signature = point.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        PreAuthorize annotation = method.getAnnotation(PreAuthorize.class);
        if (annotation == null) {
            return point.proceed();
        }

        if (!StringUtils.isEmpty(annotation.hasPermi())) {
            if (hasPermi(annotation.hasPermi())) {
                return point.proceed();
            }
            throw new PreAuthorizeException("hasPermi", new String[]{annotation.hasPermi()});
        } else if (!StringUtils.isEmpty(annotation.lacksPermi())) {
            if (lacksPermi(annotation.lacksPermi())) {
                return point.proceed();
            }
            throw new PreAuthorizeException("lacksPermi", new String[]{annotation.lacksPermi()});
        } else if (!StringUtils.isEmpty(annotation.hasAnyPermi())) {
            if (hasAnyPermi(annotation.hasAnyPermi())) {
                return point.proceed();
            }
            throw new PreAuthorizeException("hasAnyPermi", annotation.hasAnyPermi());
        } else if (!StringUtils.isEmpty(annotation.hasRole())) {
            if (hasRole(annotation.hasRole())) {
                return point.proceed();
            }
            throw new PreAuthorizeException("hasRole", new String[]{annotation.hasRole()});
        } else if (StringUtils.isEmpty(annotation.lacksRole())) {
            if (lacksRole(annotation.lacksRole())) {
                return point.proceed();
            }
            throw new PreAuthorizeException("lacksRole", new String[]{annotation.lacksRole()});
        } else if (StringUtils.isEmpty(annotation.hasAnyRoles())) {
            if (hasAnyRoles(annotation.hasAnyRoles())) {
                return point.proceed();
            }
            throw new PreAuthorizeException("hasAnyRoles", annotation.hasAnyRoles());
        }

        return point.proceed();
    }

    /**
     * 验证用户是否具备某权限
     *
     * @param permission 权限字符串
     * @return 用户是否具备某权限
     */
    public boolean hasPermi(String permission) {
        LoginUser userInfo = tokenService.getLoginUser();
        if (StringUtils.isEmpty(userInfo) || CollectionUtils.isEmpty(userInfo.getPermissions())) {
            return false;
        }
        return hasPermissions(userInfo.getAllPermissions(), permission);
    }

    /**
     * 验证用户是否不具备某权限，与 hasPermi逻辑相反
     *
     * @param permission 权限字符串
     * @return 用户是否不具备某权限
     */
    public boolean lacksPermi(String permission) {
        return hasPermi(permission) != true;
    }

    /**
     * 验证用户是否具有以下任意一个权限
     *
     * @param permissions 权限列表
     * @return 用户是否具有以下任意一个权限
     */
    public boolean hasAnyPermi(String[] permissions) {
        LoginUser userInfo = tokenService.getLoginUser();
        if (StringUtils.isEmpty(userInfo) || CollectionUtils.isEmpty(userInfo.getPermissions())) {
            return false;
        }
        Collection<String> authorities = userInfo.getAllPermissions();
        for (String permission : permissions) {
            if (permission != null && hasPermissions(authorities, permission)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断用户是否拥有某个角色
     *
     * @param role 角色字符串
     * @return 用户是否具备某角色
     */
    public boolean hasRole(String role) {
        LoginUser userInfo = tokenService.getLoginUser();
        if (StringUtils.isEmpty(userInfo) || CollectionUtils.isEmpty(userInfo.getRoles())) {
            return false;
        }
        for (String roleKey : userInfo.getRoles()) {
            if (UserConstants.ADMIN_ROLE.contains(roleKey) || roleKey.contains(role)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 验证用户是否不具备某角色，与 isRole逻辑相反。
     *
     * @param role 角色名称
     * @return 用户是否不具备某角色
     */
    public boolean lacksRole(String role) {
        return hasRole(role) != true;
    }

    /**
     * 验证用户是否具有以下任意一个角色
     *
     * @param roles 角色列表
     * @return 用户是否具有以下任意一个角色
     */
    public boolean hasAnyRoles(String[] roles) {
        LoginUser userInfo = tokenService.getLoginUser();
        if (StringUtils.isEmpty(userInfo) || CollectionUtils.isEmpty(userInfo.getRoles())) {
            return false;
        }
        for (String role : roles) {
            if (hasRole(role)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否包含权限
     *
     * @param authorities 权限列表
     * @param permission  权限字符串
     * @return 用户是否具备某权限
     */
    private boolean hasPermissions(Collection<String> authorities, String permission) {
        return authorities.stream().filter(StringUtils::hasText)
                .anyMatch(x -> UserConstants.ALL_PERMISSION.contains(x) || PatternMatchUtils.simpleMatch(permission, x));
    }
}