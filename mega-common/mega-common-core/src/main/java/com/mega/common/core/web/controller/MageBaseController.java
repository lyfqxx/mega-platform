package com.mega.common.core.web.controller;


import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.mega.common.core.web.service.IMegaBaseService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description BaseController
 * @Author Eric
 * @Date 2020年09月22日 16:20:16
 * @Version 1.0
 */
public abstract class MageBaseController<T extends Model, S extends IMegaBaseService<T>> {

    @Autowired
    protected S service;

}
