package com.mega.common.core.web.service;


import com.mega.common.core.web.model.TreeNode;

/**
 * @Description TreeNode
 * @Author Eric
 * @Date 2020年09月25日 10:05:16
 * @Version 1.0
 */
public interface Tree<T extends TreeNode> {

    /**
     * @Description 添加子节点
     * @Author Eric
     * @Date 2020年09月25日 10:05:16
     * @Version 1.0
     */
    void add(T node);
}
