package com.mega.common.core.web.model;

import com.mega.common.core.web.service.Tree;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description TreeNode
 * @Author Eric
 * @Date 2020年09月25日 10:05:16
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class TreeNode<V, T extends TreeNode> implements Tree<T> {
    protected V code;
    protected V parentCode;
    protected List<T> children = new ArrayList<>();

    @Override
    public void add(T node) {
        this.children.add(node);
    }
}
