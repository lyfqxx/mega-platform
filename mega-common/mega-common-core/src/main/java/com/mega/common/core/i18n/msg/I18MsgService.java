package com.mega.common.core.i18n.msg;

import com.mega.common.core.domain.R;
import com.mega.common.core.exception.BusinessException;
import com.mega.common.core.i18n.config.I18nService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @description: 国际化
 * @author: Eric
 * @date: 2020/9/22 10:52
 */
@Slf4j
@AllArgsConstructor
public class I18MsgService implements ServerExceptionMsgService {

    private final I18nService i18nService;

    @Override
    public R getExceptionMessage(BusinessException e) {
        String msg;
        if (e.getCode() == null) {
            msg = e.getMessage();
        } else {
            if (e.getArgs() != null && e.getArgs().length > 0) {
                msg = i18nService.getMessage(e.getCode(), e.getArgs());
            } else {
                msg = i18nService.getMessage(e.getCode());
            }
        }
        return R.fail(msg);
    }
}
