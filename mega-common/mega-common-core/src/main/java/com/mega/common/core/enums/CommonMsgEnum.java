package com.mega.common.core.enums;

/**
 * @description: 通用异常信息
 * @author: Eric
 * @date: 2020/9/22 9:11
 */
public enum CommonMsgEnum implements BaseMsgEnum {

    //授权已过期
    AUTHENTICATION_EXPIRED("AUTHENTICATION_EXPIRED"),
    //认证失败，缺少授权信息
    NO_TOKEN_REQUEST("NO_TOKEN_REQUEST"),
    //认证失败
    AUTHENTICATION_FAIL("AUTHENTICATION_FAIL"),
    //访问受限
    FORBIDDEN("FORBIDDEN"),
    //重复提交
    REPEATSUBMIT_ERROR("REPEATSUBMIT_ERROR"),
    //名称已存在
    NAME_REPEAT("NAME_REPEAT"),
    //编码已存在
    CODE_REPEAT("CODE_REPEAT"),
    //数据不存在
    DATE_NOT_EXIST("DATE_NOT_EXIST"),
    //无效状态
    STATUS_NOT_VALID("STATUS_NOT_VALID"),
    //生成编码失败：找不到表名
    CREATE_CODE_ERROR("CREATE_CODE_ERROR"),
    //操作成功
    SUCCESS("SUCCESS"),
    //操作失败
    FAIL("FAIL"),
    //系统未知错误
    SYSTEM_UNKNOWN_ERROR("SYSTEM_UNKNOWN_ERROR"),
    ;

    private String code;

    CommonMsgEnum(String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return this.code;
    }
}
