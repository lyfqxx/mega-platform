package com.mega.common.core.exception.config;

import com.mega.common.core.i18n.config.I18nService;
import com.mega.common.core.i18n.msg.ClientMsgService;
import com.mega.common.core.i18n.msg.I18MsgService;
import com.mega.common.core.i18n.msg.LocalMsgService;
import com.mega.common.core.i18n.msg.ServerExceptionMsgService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.mega.common.core.enums.MsgTypeEnum.CLIENT;
import static com.mega.common.core.enums.MsgTypeEnum.I18N;

/**
 * @Description 异常信息
 * @Author Eric
 * @Date 2020年09月30日 14:44
 * @Version 1.0
 */
@Slf4j
@Configuration
public class ExceptionMsgConfig {

    @Autowired
    private ExceptionMsgProperties propertiesConfig;

    @Bean
    @RefreshScope
    public ServerExceptionMsgService serverExceptionMsgService(I18nService i18nService) {
        if (propertiesConfig.getType() != null) {
            if (propertiesConfig.getType().equals(I18N)) {
                return new I18MsgService(i18nService);
            }
            if (propertiesConfig.getType().equals(CLIENT)) {
                return new ClientMsgService();
            }
        }
        return new LocalMsgService();
    }
}
