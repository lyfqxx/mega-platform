package com.mega.common.core.enums;

/**
 * 用户状态
 *
 * @author mega
 */
public enum CommonStatus {

    NORMAL("NORMAL", "正常"),
    DISABLED("DISABLED", "停用"),
    BLOCKED("BLOCKED", "锁定");

    private final String code;
    private final String info;

    CommonStatus(String code, String info) {
        this.code = code;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }
}
