package com.mega.common.core.web.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description BaseEntity
 * @Author Eric
 * @Date 2020年09月22日 17:05:16
 * @Version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public abstract class MegaBaseEntityNone<T extends Model<T>> extends Model<T> implements Serializable {

    private static final long serialVersionUID = 1L;

}
