package com.mega.common.core.web.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class KeyValue implements Serializable {

    private static final long serialVersionUID = 1L;

    private String key;

    private String value;
}
