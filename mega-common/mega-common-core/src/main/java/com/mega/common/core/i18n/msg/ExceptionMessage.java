package com.mega.common.core.i18n.msg;

import lombok.Getter;

/**
 * @description: 异常消息类
 * @author: Eric
 * @date: 2020/9/22 10:55
 */
@Getter
public class ExceptionMessage {

    /**
     * 编码
     */
    private String code;

    /**
     * msg
     */
    private String msg;

    public ExceptionMessage(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
