package com.mega.common.core.enums;

/**
 * @description: 基础异常枚举类
 * @author: Eric
 * @date: 2020/9/22 9:08
 */
public interface BaseMsgEnum {

    /**
     * @Description 获取异常code
     * @Return enum名称
     */
    String getCode();
}