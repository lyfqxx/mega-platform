package com.mega.common.core.utils;

import com.mega.common.core.i18n.config.I18nService;

/**
 * @description: 获取i18n资源文件
 * @author: Eric
 * @date: 2020/9/22 9:30
 */
public class MessageUtils {

    /**
     * 根据消息键和参数 获取消息 委托给spring messageSource
     *
     * @param code 消息键
     * @param args 参数
     * @return 获取国际化翻译值
     */
    public static String message(String code, Object... args) {
        I18nService i18nService = SpringUtils.getBean(I18nService.class);
        return i18nService.getMessage(code, args);
    }
}
