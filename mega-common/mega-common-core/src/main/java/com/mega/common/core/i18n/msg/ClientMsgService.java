package com.mega.common.core.i18n.msg;

import com.mega.common.core.domain.R;
import com.mega.common.core.exception.BusinessException;

/**
 * @Description 客户端异常信息
 * @Author Eric
 * @Date 2020年09月30日 14:44
 * @Version 1.0
 */
public class ClientMsgService implements ServerExceptionMsgService {

    @Override
    public R getExceptionMessage(BusinessException e) {
        return R.fail(e.getArgs(), e.getCode());
    }
}