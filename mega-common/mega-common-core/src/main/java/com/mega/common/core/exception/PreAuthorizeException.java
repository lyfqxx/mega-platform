package com.mega.common.core.exception;

import lombok.Getter;
import lombok.ToString;

/**
 * 权限异常
 *
 * @author mega
 */
@Getter
@ToString
public class PreAuthorizeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private String code;

    /**
     * 参数
     */
    private String[] args;

    public PreAuthorizeException() {
    }

    public PreAuthorizeException(String message) {
        super(message);
    }

    public PreAuthorizeException(String code, String[] args) {
        this.code = code;
        this.args = args;
    }

    public PreAuthorizeException(String message, String code, String[] args) {
        super(message);
        this.code = code;
        this.args = args;
    }
}
