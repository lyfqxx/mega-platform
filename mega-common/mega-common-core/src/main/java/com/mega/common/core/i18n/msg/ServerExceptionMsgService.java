package com.mega.common.core.i18n.msg;

import com.mega.common.core.domain.R;
import com.mega.common.core.exception.BusinessException;

/**
 * @description: 服务异常信息服务类
 * @author: Eric
 * @date: 2020/9/22 10:58
 */
public interface ServerExceptionMsgService {

    /**
     * @param e
     * @Description 获取异常消息
     * @Author Eric
     * @Date 2020/9/22 10:58
     * @Return
     */
    R getExceptionMessage(BusinessException e);
}
