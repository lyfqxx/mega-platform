package com.mega.common.core.exception;

import com.mega.common.core.enums.BaseMsgEnum;
import lombok.Getter;
import lombok.ToString;

/**
 * @description: 业务异常
 * @author: Eric
 * @date: 2020/9/22 10:42
 */
@Getter
@ToString
public class BusinessException extends RuntimeException {

    /**
     * 错误码
     */
    private String code;

    /**
     * 参数
     */
    private String[] args;

    /**
     * @param message 消息
     * @Author Eric
     * @Date 2020年09月22日 14:45:56
     * @Return
     */
    public BusinessException(String message) {
        super(message);
    }

    /**
     * @param code    错误code
     * @param message 消息
     * @Description
     * @Author Eric
     * @Date 2020年09月22日 14:45:56
     * @Return
     */
    public BusinessException(String code, String message) {
        super(message);
        this.code = code;
    }

    /**
     * @param exceptionEnum 异常枚举
     * @Description
     * @Author Eric
     * @Date 2020年09月22日 14:45:56
     * @Return
     */
    public BusinessException(BaseMsgEnum exceptionEnum) {
        this.code = exceptionEnum.getCode();
    }

    /**
     * @param exceptionEnum 异常枚举
     * @param args          参数
     * @Description
     * @Author Eric
     * @Date 2020年09月22日 14:45:56
     * @Return
     */
    public BusinessException(BaseMsgEnum exceptionEnum, String... args) {
        this.code = exceptionEnum.getCode();
        this.args = args;
    }
}
