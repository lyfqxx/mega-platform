package com.mega.common.core.web.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description
 * @Author Eric
 * @Date 2020年09月22日 16:05:16
 * @Version 1.0
 */
public class MegaBaseServiceImpl<M extends BaseMapper<T>, T extends Model> extends ServiceImpl<M, T> implements IMegaBaseService<T> {

}
