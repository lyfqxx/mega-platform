package com.mega.common.core.i18n.msg;

import com.mega.common.core.domain.R;
import com.mega.common.core.exception.BusinessException;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;

/**
 * @Description 本地异常信息
 * @Author Eric
 * @Date 2020年09月30日 14:44
 * @Version 1.0
 */
public class LocalMsgService implements ServerExceptionMsgService {

    @Override
    public R getExceptionMessage(BusinessException e) {
        return R.fail(format(e.getMessage(), e.getArgs()));
    }

    private String format(String msg, String... args) {
        if (!ObjectUtils.isEmpty(args)) {
            return MessageFormat.format(msg, args);
        }
        return msg;
    }
}