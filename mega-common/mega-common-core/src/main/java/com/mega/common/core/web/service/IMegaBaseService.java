package com.mega.common.core.web.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description
 * @Author Eric
 * @Date 2020年09月22日 16:05:16
 * @Version 1.0
 */
public interface IMegaBaseService<T> extends IService<T> {

}
