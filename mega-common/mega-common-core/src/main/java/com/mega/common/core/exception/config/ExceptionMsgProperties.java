package com.mega.common.core.exception.config;

import com.mega.common.core.enums.MsgTypeEnum;
import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * @Description 参数配置
 * @Author Eric
 * @Date 2020年09月30日 14:44
 * @Version 1.0
 */
@Data
@Configuration
@RefreshScope
@ConditionalOnExpression("!'${mega.exception}'.isEmpty()")
@ConfigurationProperties(prefix = "mega.exception.msg")
public class ExceptionMsgProperties {

    /**
     * 类型。可选I18N、CLIENT、LOCAL
     */
    private MsgTypeEnum type;

}
