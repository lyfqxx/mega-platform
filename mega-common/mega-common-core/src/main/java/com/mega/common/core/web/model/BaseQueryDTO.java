package com.mega.common.core.web.model;

import com.baomidou.mybatisplus.core.enums.SqlKeyword;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mega.common.core.constant.Constants;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 分页查询通用参数类
 */
public class BaseQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 当前页数,默认1
     */
    @ApiModelProperty(value = "当前页数,默认1")
    @JsonIgnore
    protected long page = 1;

    /**
     * 每页数量,默认10
     */
    @ApiModelProperty(value = "每页数量,默认10")
    @JsonIgnore
    protected long size = 10;

    /**
     * 跳过的数量
     */
    @ApiModelProperty(value = "offset,默认0", hidden = true)
    @JsonIgnore
    protected long offset = 0;

    /**
     * 排序列
     */
    @ApiModelProperty(value = "排序列，默认创建时间")
    @JsonIgnore
    protected String sortColumn = Constants.DEFAULT_SORT_COLUMN;

    /**
     * 排序规则,ASC和DESC
     */
    @ApiModelProperty(value = "排序规则,默认倒序")
    @JsonIgnore
    protected String sortRule = SqlKeyword.DESC.getSqlSegment();

    public long getPage() {
        return page;
    }

    public void setPage(long page) {
        this.page = page;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getOffset() {
        return (this.page - 1) * size;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    public String getSortRule() {
        return sortRule;
    }

    public void setSortRule(String sortRule) {
        this.sortRule = sortRule;
    }
}
