package com.mega.common.core.enums;

/**
 * @Description 异常枚举
 * @Author Eric
 * @Date 2020年09月30日 14:44
 * @Version 1.0
 */
public enum MsgTypeEnum {
    // 本地
    LOCAL,
    // 客户端
    CLIENT,
    // i18n国际化
    I18N;
}