package com.mega.common.core.constant;

/**
 * 用户常量信息
 *
 * @author mega
 */
public interface UserConstants {

    /**
     * 树图默认根节点编码
     */
    String DEFAULT_TREE_ROOT_CODE = "0";

    /**
     * 用户名长度限制
     */
    int USERNAME_MIN_LENGTH = 2;

    int USERNAME_MAX_LENGTH = 20;

    /**
     * 密码长度限制
     */
    int PASSWORD_MIN_LENGTH = 5;

    int PASSWORD_MAX_LENGTH = 20;

    /**
     * 管理员名称
     */
    String ADMIN_NAME = "admin";

    /**
     * 管理员角色
     */
    String ADMIN_ROLE = "admin";

    /**
     * 全部权限
     */
    String ALL_PERMISSION = "*:*:*";
}
