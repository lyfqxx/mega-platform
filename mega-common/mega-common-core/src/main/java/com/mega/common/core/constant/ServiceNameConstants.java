package com.mega.common.core.constant;

/**
 * 服务名称
 *
 * @author mega
 */
public interface ServiceNameConstants {
    /**
     * 认证服务的serviceid
     */
     String AUTH_SERVICE = "mega-auth";

    /**
     * 系统模块的serviceid
     */
     String SYSTEM_SERVICE = "mega-system";
}
