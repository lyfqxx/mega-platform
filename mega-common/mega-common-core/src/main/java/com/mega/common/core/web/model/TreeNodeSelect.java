package com.mega.common.core.web.model;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Description TreeNodeSelect
 * @Author Eric
 * @Date 2020年09月25日 10:05:16
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class TreeNodeSelect extends TreeNode<String, TreeNodeSelect> {

    /**
     * 名称
     */
    protected String label;

}
