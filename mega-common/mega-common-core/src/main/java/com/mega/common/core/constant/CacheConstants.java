package com.mega.common.core.constant;

/**
 * 缓存的key 常量
 *
 * @author mega
 */
public interface CacheConstants {
    /**
     * 令牌自定义标识
     */
    String HEADER = "Authorization";

    /**
     * 令牌前缀
     */
    String TOKEN_PREFIX = "Bearer ";

    /**
     * 权限缓存前缀
     */
    String LOGIN_TOKEN_KEY = "login_tokens:";

    /**
     * 用户ID字段
     */
    String DETAILS_USER_ID = "user_id";

    /**
     * 用户名字段
     */
    String DETAILS_USERNAME = "username";

    /**
     * 防重提交 redis key
     */
    String REPEAT_SUBMIT_KEY = "repeat_submit:";

    /**
     * 数据字典
     */
    String DICTIONARY = "dictionary:";

    /**
     * 国际化
     */
    String I18N = "i18n:";

    /**
     * 表编码
     */
    String TABLE_CODE = "table_codes:";
}
