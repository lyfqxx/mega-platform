package com.mega.system.api;

import com.mega.common.core.constant.ServiceNameConstants;
import com.mega.common.core.domain.R;
import com.mega.system.api.factory.RemoteLogFallback;
import com.mega.system.api.vo.LogVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 日志服务
 *
 * @author mega
 */
@FeignClient(name = ServiceNameConstants.SYSTEM_SERVICE, url = "${mega.feign-url.system}", fallback = RemoteLogFallback.class)
public interface RemoteLogService {

    /**
     * 保存系统日志
     *
     * @param logVO 日志实体
     * @return 结果
     */
    @PostMapping("/api/log")
    R<Boolean> saveLog(@RequestBody LogVO logVO);
}
