package com.mega.system.api.factory;

import com.mega.common.core.domain.R;
import com.mega.system.api.RemoteLogService;
import com.mega.system.api.vo.LogVO;
import org.springframework.stereotype.Component;

/**
 * 日志服务降级处理
 *
 * @author mega
 */
@Component
public class RemoteLogFallback implements RemoteLogService {

    @Override
    public R<Boolean> saveLog(LogVO sysOperLog) {
        return null;
    }

}
