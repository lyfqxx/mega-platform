package com.mega.system.api.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mega.common.core.web.model.TreeNodeSelect;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value = "模块VO对象")
public class ModuleVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "模块编码")
    private String moduleCode;

    @ApiModelProperty(value = "模块名称")
    private String moduleName;

    @ApiModelProperty(value = "模块URL")
    private String moduleUrl;

    @ApiModelProperty(value = "菜单信息")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<TreeNodeSelect> menuData;
}
