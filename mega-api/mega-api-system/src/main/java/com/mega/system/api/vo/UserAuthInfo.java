package com.mega.system.api.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 用户及相关权限信息
 *
 * @author mega
 */
@Data
@ApiModel(value = "用户及相关权限VO")
public class UserAuthInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户信息")
    private UserInfo user;

    @ApiModelProperty(value = "角色列表")
    private Set<String> roles;

    @ApiModelProperty(value = "用户模块列表")
    private List<ModuleVO> modules;

    @ApiModelProperty(value = "用户模块权限列表")
    private Map<String, Set<String>> permissions;

    @ApiModelProperty(value = "用户权限列表")
    @JsonIgnore
    private Set<String> allPermissions;
}
